from subprocess import run
from subprocess import check_output
import sys
import os
from colorama import Fore as f
import colorama
colorama.init()
import time
from datetime import datetime
import shutil

def logger(func):
	def out(msg):
		print(f"{f.GREEN}[{datetime.now().strftime('%H:%M:%S')}]{f.RESET} {msg}")

	def wrapper(*args, **kwargs):
		print(f"{f.GREEN}[{datetime.now().strftime('%H:%M:%S')}]{f.RESET} {f.LIGHTBLUE_EX}{func.__name__}{f.RESET}")
		start = time.time()
		val = func(*args, **kwargs, out=out)
		end = time.time()
		print(f"{f.GREEN}[{datetime.now().strftime('%H:%M:%S')}]{f.RESET} {f.LIGHTBLUE_EX}{func.__name__}{f.RESET} {f.LIGHTGREEN_EX}Completed in {round(end-start, 2)}s{f.RESET}")
		return val


	return wrapper


def error(msg):
	print(f.RED + msg + f.RESET)
	sys.exit(1)

@logger
def create_env(out=None):
	if os.path.exists("env"):
		error(f"env already exists, delete {os.path.abspath('env')}")
	run([sys.executable, "-m", "virtualenv", "env"], check=True, capture_output=True, text=True)

@logger
def install_deps(out=None):
	for lib in ["colorama==0.4.4", "requests==2.27.1", "sql-1-py3-none-any.whl", "pyinstaller==5.0"]:
		out(f"Installing {lib}")
		if run([".\\env\\Scripts\\python.exe", "-m", "pip", "install", lib], check=True, capture_output=True, text=True).returncode != 0:
			error(f"Failed to install {lib}")

@logger
def compile(path, out=None):
	out("Running pyinstaller.")
	if run([".\\env\\Scripts\\pyinstaller.exe", "wordle.py", "-i", "app.ico", "--add-data", "words.db;.", "--add-data", ".\\driver\\sqlite3.exe;.\\driver\\."], check=True, capture_output=True, text=True).returncode != 0:
		error("pyinstaller failed")
	
	os.chdir(os.path.dirname(os.path.abspath(__file__)))
	os.remove("wordle.spec")

	out("Compiling into installer.")

	if run([path, ".\\InstallerScripts\\installer.iss", "/Q"], shell=True).returncode != 0:
		error("iscc failed")

	out("Compiled to " + os.path.abspath('.\\Wordle\\WordleSetup.exe'))

	out("Compressing.")

	os.chdir(os.path.dirname(os.path.abspath(__file__)))
	if run([".\\driver\\7z.exe", "a", ".\\Wordle\\wordle.zip", ".\\dist\\wordle\\*", "-tzip"], check=True, capture_output=True, text=True).returncode != 0:
		error("7z failed")

	out("Compressed to " + os.path.abspath('.\\Wordle\\wordle.zip'))
	
	#shutil.move(".\\dist\\wordle.zip", ".\\Wordle\\wordle.zip")

	out("Cleaning up.")

	for delete in [["rmdir", "build", "/s", "/q"], ["rmdir", "dist", "/s", "/q"]]:
		if run(delete, shell=True, check=True, capture_output=True, text=True).returncode != 0:
			error("Failed to delete build/dist")


@logger
def main(out=None):
	out("Checking python version...")
	if sys.version_info[:2] == (3, 10):
		out("Python version is correct")
	else:
		error("Python version is not correct, please install python 3.10")

	out("Checking for OS...")
	if sys.platform != "win32":
		error("This script is only for Windows")
	else:
		out("OS is correct")

	out("Checking for dependencies...")
	output = run([sys.executable, "-m" ,"pip", "list"], check=True, capture_output=True, text=True).stdout
	if "virtualenv" not in output:
		error("virtualenv is not installed, please install it (pip install virtualenv)")
	PATH = os.environ["PATH"].split(";")
	check = False
	isccpath = ""
	for i in PATH:
		if os.path.exists(i) != True:
			continue
		if os.path.exists(f"{i}\\iscc.exe"):
			isccpath = f"{i}\\iscc.exe"
			check = True
			break
	if check==False:
		error("iscc.exe is not in your PATH or is not installed, please install/add it (https://jrsoftware.org/isdl.php)")

	if os.path.exists("Wordle"):
		shutil.rmtree("Wordle")
	if os.path.exists("build"):
		shutil.rmtree("build")
	if os.path.exists("dist"):
		shutil.rmtree("dist")

	out("Creating a virtual environment...")
	create_env()
	out("Virtual environment created")

	out("Installing dependencies...")
	install_deps()
	out("Dependencies installed")

	out("Compiling...")
	compile(isccpath)
	out(f"Compiled to {os.path.abspath('Wordle')}")

	run(["rmdir", ".\\env", "/q", "/s"], shell=True, check=True, capture_output=True, text=True)
	out("Done")


main()