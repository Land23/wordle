# Wordle
### Compiling instructions
1. Make sure you are using Windows 10/11
2. Install python 3.10
3. Download the source code of this repository as a ZIP
4. Extract the ZIP file
5. Run "compile.py" and follow the instructions it gives you
6. Once the compilation is done, there will be a output folder called "Wordle", which contains the compiled wordle.