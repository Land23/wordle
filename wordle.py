from sql import sql
from random import randint as rand
from random import choice as ch
import argparse
import sys
import hashlib
import os
import requests
from colorama import Fore as f
import colorama
import re
from datetime import datetime
from getpass import getpass
import string as all_letters
from subprocess import run
import shutil
OriginalPath = os.getcwd()
os.chdir(os.path.dirname(os.path.realpath(__file__)))

colorama.init()



def hash(data):
	encoded = data.encode()
	hasher = hashlib.sha512()
	hasher.update(encoded)
	return hasher.hexdigest()

def ArgParser(args):
	parser = argparse.ArgumentParser(description="A wordle game.")
	parser.add_argument("-c", "--create", help="Create a new account.", action="store_true")
	parser.add_argument("-l", "--login", help="Login to an existing account.", action="store_true")
	parser.add_argument("-o", "--log-out", help="Log out of an existing account.", action="store_true")
	parser.add_argument("-d", "--delete", help="Delete an existing account.", action="store_true")
	parser.add_argument("-li", "--list", help="List all accounts and thier stats.", action="store_true")
	parser.add_argument("-ch", "--change", help="Change your password or username.", choices=["username", "password"], type=str)
	parser.add_argument("-p", "--play", help="Play the game.", action="store_true")
	parser.add_argument("-hi", "--history", help="Show history.", action="store_true")
	parser.add_argument("-w", "--wipe-data", help="Wipes all data.", action="store_true")
	parser.add_argument("-u", "--uninstall", help="Uninstall wordle.", action="store_true")
	parser.add_argument("-da", "--data", help="Import/output data", choices=["import", "export"], type=str)

	parser.add_argument("-m", "--manage-database", help="A way to manually manage the database (ONLY FOR ADVANCED USERS, COULD CORRUPT YOUR DATA)", action="store_true")
	parser.add_argument("-dg", "--delete-game", help="Delete a game from the history/database.", action="store_true")

	return parser.parse_args(args)

def DownloadData():
	out("Downloading words.")
	link = "https://raw.githubusercontent.com/tabatkins/wordle-list/main/words"
	try:
		words = requests.get(link, timeout=5)
	except requests.Timeout or ConnectionRefusedError or ConnectionError or ConnectionResetError or ConnectionAbortedError:
		sys.exit("Unstable internet")
	return words.text.split("\n")

def is_logged_in(s) -> bool:
	return (s.get_all_data("logged_in")) != []

def out(msg):
	print(f"{f.LIGHTMAGENTA_EX}LOG: {msg}{f.RESET}")

def inp(msg):
	return input(f"{f.LIGHTMAGENTA_EX}INPUT: {msg}{f.RESET}")

def getpassword(msg):
	return getpass(f"{f.LIGHTMAGENTA_EX}INPUT: {msg}{f.RESET}")

def main():
	args = ArgParser(sys.argv[1:])

	if args.uninstall:
		if os.path.exists(".\\unins000.exe"):
			os.execvp(".\\unins000.exe", [".\\unins000.exe"])


	out("Connecting to database.")
	try:
		s = sql("data.db")
	except Exception:
		sys.exit("Unable to open database.\nMost likely no permission.")
	s.add_table("accounts", {"username": s.string_unique, "password": s.string})
	s.add_table("user_data", {"username": s.string, "history": s.string, "guesses": s.integer, "word": s.string, "time": s.string, "id": s.integer})
	s.add_table("stats", {"username": s.string_unique, "right": s.integer, "wrong": s.integer})
	s.add_table("logged_in", {"username": s.string_unique})

	if args.create:
		user = inp("Username ? ")
		if (user.replace(" ", "") == ""):
			out("Invalid username.")
			sys.exit()
		passw = getpassword("Password ? ")
		if (s.get_data("accounts", {"username": user})) != []:
			out("Username already exists.")
			sys.exit()
		s.insert("accounts", {"username": user, "password": hash(passw)})
		s.insert("stats", {"username": user, "right": 0, "wrong": 0})
		out("Account created.")
		out("Now use the --login option to login.")

	if args.log_out:

		if is_logged_in(s) == False:
			out("You are not logged in.")
			sys.exit()

		s.delete_value("logged_in", {"username": s.get_all_data("logged_in")[0][0]})
		out("Logged out.")

	if args.login:
		if is_logged_in(s):
			out("You are already logged in.")
			out("Please use --log-out")
			sys.exit()
		user = inp("Username ? ")
		passw = getpassword("Password ? ")
		if (s.get_data("accounts", {"username": user})) == []:
			out("Username invalid.")
			sys.exit()
		saved = s.get_data("accounts", {"username": user})[0]
		if saved[1] != hash(passw):
			out("Password invalid.")
			sys.exit()
		s.insert("logged_in", {"username": user})
		out("Logged in.")

	if args.delete:
		user = inp("Username ? ")
		if (s.get_data("accounts", {"username": user})) == []:
			out("Username invalid.")
			sys.exit()

		if (s.get_data("logged_in", {"username": user})) != []:
			out("Log out of the account to delete.")
			sys.exit()

		passw = getpassword("Password ? ")
		if s.get_data("accounts", {"username": user})[0][1] != hash(passw):
			out("Password invalid.")
			sys.exit()

		s.delete_value("accounts", {"username": user})
		s.delete_value("stats", {"username": user})
		out("Account deleted.")

	if args.list:
		if (is_logged_in(s)):
			user = s.get_all_data("logged_in")[0][0]
		else:
			user = ""
		saved = s.get_all_data("accounts")
		print("")
		for account in saved:
			stats = s.get_data("stats", {"username": account[0]})[0]
			if (user != account[0]):
				print(
f'''Account: {account[0]}
	Total games done: {stats[1]+stats[2]}
	Games won: {stats[1]}
	Games lost: {stats[2]}
''')
			else:
				print(
f'''Account: {account[0]} (Logged in)
	Total games done: {stats[1]+stats[2]}
	Games won: {stats[1]}
	Games lost: {stats[2]}
''')
		if (saved == []):
			out("No accounts found.")

	if args.change:
		if is_logged_in(s) == False:
			out("You are not logged in.")
			sys.exit()
		user = s.get_all_data("logged_in")[0][0]
		passw = getpassword("Password ? ")
		saved = s.get_data("accounts", {"username": user})[0]
		if saved[1] != hash(passw):
			out("Password invalid.")
			sys.exit()
		
		if args.change == "username":
			new_user = inp("New username: ")
			if (s.get_data("accounts", {"username": new_user})) != []:
				out("Username already exists.")
				sys.exit()
			old_stats = s.get_data("stats", {"username": user})[0]
			s.delete_value("logged_in", {"username": user})
			s.delete_value("accounts", {"username": user})
			s.delete_value("stats", {"username": user})
			s.insert("accounts", {"username": new_user, "password": hash(passw)})
			s.insert("stats", {"username": new_user, "right": old_stats[1], "wrong": old_stats[2]})

			out("Username changed.")
			out("Log back in with the new username.")

		if args.change == "password":
			new_passw = getpassword("New password: ")

			s.delete_value("logged_in", {"username": user})
			s.delete_value("accounts", {"username": user})
			s.insert("accounts", {"username": user, "password": hash(new_passw)})

			out("Password changed.")
			out("Log back in with the new password.")

	if args.play:
		if is_logged_in(s) == False:
			out("You are not logged in.")
			sys.exit()

		
		user = s.get_all_data("logged_in")[0][0]

		if (os.path.exists("words.db") != True):
			out("Word database does not exist, please redownload at https://gitlab.com/Land23/wordle/-/raw/0bb9fc1f144a99cad3fd9f273d7cbb11b7110cea/words.db")
			sys.exit()
		word_database = sql("words.db")
		data = word_database.get_all_data("words")
		words = []
		for i in data:
			words.append(i[0])

		

		r = re.compile("([a-z]){5}")
		#words = []
		#for i in DownloadData():
		#	i = i.replace("\n", "")
		#	mat = r.match(i)
		#	if (mat != None):
		#		if (mat.group() == i):
		#			words.append(i)

		word = ch(words)
		os.system("cls")
		print(f"{f.CYAN}Logged in as {user}{f.RESET}")
		lives = 6
		print("Type 'exit' to exit")
		hist = ""
		while lives != 0:
			hist += "Lives: " + str(lives) + "\n"
			print("Lives: " + str(lives))
			while True:
				guess = inp("Guess word ? ").lower()
				hist += f"{f.LIGHTMAGENTA_EX}Guess word ? {f.RESET}" + guess + "\n"
				if (guess == "exit"):
					sys.exit()
				if (r.match(guess) != None):
					if (r.match(guess).group() == guess):
						if (guess in words):
							break
						else:
							hist += f"{f.LIGHTMAGENTA_EX}Not in list of words\n{f.RESET}"
							out("Not in list of words")
					else:
						hist += f"{f.LIGHTMAGENTA_EX}Invalid characters or not 5 letters\n{f.RESET}"
						out("Invalid characters or not 5 letters")
				else:
					hist += f"{f.LIGHTMAGENTA_EX}Invalid characters or not 5 letters\n{f.RESET}"
					out("Invalid characters or not 5 letters")
			if (guess != word):
				lives -= 1
			string = ""

			d = [f.RED, f.RED, f.RED, f.RED, f.RED]
			#temp_word = word
#
			#temp_index = 0
			#for i in range(len(d)):
			#	
			#	if guess[i] == word[i]:
			#		
			#		d[i] = f.GREEN
			#		temp_word = temp_word[:temp_index] + temp_word[temp_index+1:]
			#		print(temp_word, temp_index)
			#		temp_index -= 1
			#	temp_index += 1
#
			#temp_index = 0
			#for i in range(len(d)):
			#	if guess[i] in temp_word and guess[i] != word[i]:
			#		d[i] = f.YELLOW
			#		temp_word = temp_word[:temp_index] + temp_word[temp_index+1:]
			#		print(temp_word, temp_index)
			#		temp_index -= 1
			#	temp_index += 1

			#letters = {"a": [0], "b": [0], "c": [0], "d": [0], "e": [0], "f": [0], "g": [0], "h": [0], "i": [0], "j": [0], "k": [0], "l": [0], "m": [0], "n": [0], "o": [0], "p": [0], "q": [0], "r": [0], "s": [0], "t": [0], "u": [0], "v": [0], "w": [0], "x": [0], "y": [0], "z": [0]}
#
			#for i in range(len(word)):
			#	letters[word[i]][0] = letters[word[i]][0]+1
			#	letters[word[i]].append(i)
			#for i in list(letters.keys()):
			#	if letters[i] == [0]:
			#		letters.pop(i)
			#arr = list(letters.keys())
			#temp_guess = word
			##for letter in arr:
			##	if letter in temp_guess:
			##		print(temp_guess)
			##		for index in letters[letter][1:]:
			##			if guess[index] == letter:
			##				d[index] = f.GREEN
			##				temp_guess = temp_guess[:index] + " " + temp_guess[index+1:]
			##for letter in arr:
			##	if letter in temp_guess:
			##		print(temp_guess)
			##		for test in range(len(d)):
			##			if d[test] != f.GREEN and guess[test] == letter:
			##				d[test] = f.YELLOW
			##				print(test)
			##				temp_guess = temp_guess[:letters] + " " + temp_guess[test+1:]
#
#
			#for letter in range(len(guess)):
			#	if guess[letter] in temp_guess:
					
			for letter in range(len(guess)):
				if guess[letter] in word:
					d[letter] = f.YELLOW
				if guess[letter] == word[letter]:
					d[letter] = f.GREEN

			temp = []
			for i in range(len(d)):
				temp.append(f"{d[i]}{guess[i].upper()}{f.RESET}")
			string = "   ".join(temp)

			string += f.RESET
			hist += string + "\n"
			print(string)
			if (guess == word):
				hist += f"\n{f.GREEN}CORRECT!{f.RESET}\n"
				print(f"\n{f.GREEN}CORRECT!{f.RESET}")
				break

		if (lives == 0):
			hist += f"The correct word was '{word}'" + "\n"
			print(f"The correct word was '{word}'")
			old_data = s.get_data("stats", {"username": user})[0]
			s.delete_value("stats", {"username": user})
			s.insert("stats", {"username": user, "right": old_data[1], "wrong": old_data[2]+1})
		else:
			hist += "Good Job!\n"
			print("Good Job!")
			old_data = s.get_data("stats", {"username": user})[0]
			s.delete_value("stats", {"username": user})
			s.insert("stats", {"username": user, "right": old_data[1]+1, "wrong": old_data[2]})

		time = datetime.now()
		time = time.strftime("%m-%d-%Y %H:%M:%S")

		next_id = len(s.get_data("user_data", {"username": user}))+1

		datatosave = {"username": user, "history": hist, "guesses": 6-lives+1, "word": word, "time": time, "id": next_id}
		s.insert("user_data", datatosave)

	if args.history:
		if (is_logged_in(s)) == False:
			out("Not logged in.")
			sys.exit()
		user = s.get_all_data("logged_in")[0][0]
		data = s.get_data("user_data", {"username": user})
		if data == []:
			out("No games have been played.")
			sys.exit()
		
		formatted_data = {}
		for entry in range(len(data)):
			formatted_data[data[entry][5]] = data[entry]

		data = formatted_data

		out("Type in a game's ID to delete the game or type 'no' to exit\n")
		print(f"{f.LIGHTBLUE_EX}ENTRIES:{f.RESET}")
		for entry in range(len(data)):
			entry = entry+1
			if data[entry] == (user, "deleted", 0, "deleted", "deleted", entry):
				print(f"{f.LIGHTBLUE_EX}ID: {f.YELLOW}{entry}{f.RESET}{f.LIGHTBLUE_EX} {' ' * (8-len(str(entry)))}Status: {f.RED}GAME DELETED{f.RESET}")
			else:
				if (data[entry][2] < 7):
					print(f"{f.LIGHTBLUE_EX}ID: {f.YELLOW}{entry}{f.LIGHTBLUE_EX} {' ' * (8-len(str(entry)))}Guesses: {f.YELLOW}{data[entry][2]}{f.LIGHTBLUE_EX} {' ' * (8-len(str(data[entry][2])))}Word: {f.YELLOW}{data[entry][3]}{f.LIGHTBLUE_EX}    Time: {f.YELLOW}{data[entry][4]}{f.RESET}")
				else:
					print(f"{f.LIGHTBLUE_EX}ID: {f.YELLOW}{entry}{f.LIGHTBLUE_EX} {' ' * (8-len(str(entry)))}Guesses: {f.YELLOW}{'Wrong'}{f.LIGHTBLUE_EX} {' ' * (8-len('Wrong'))}Word: {f.YELLOW}{data[entry][3]}{f.LIGHTBLUE_EX}    Time: {f.YELLOW}{data[entry][4]}{f.RESET}")

		id = inp("ID ? ")
		if (id == "no"):
			sys.exit()
		r = re.compile("[0-9]{1,}")
		if r.match(id) == None:
			out("Invalid ID")
			sys.exit()
		if r.match(id).group() != id:
			out("Invalid ID")
			sys.exit()
		if (int(id) <= len(data)) == False or int(id) < 1:
			out("Invalid ID")
			sys.exit()
		
		os.system("cls")
		print(f"{f.BLUE}History for game {int(id)}:\n{f.RESET}")
		print(data[int(id)][1])

	if (args.wipe_data):

		let = list(all_letters.ascii_lowercase + all_letters.ascii_uppercase + all_letters.punctuation)
		sequence = ""
		for i in range(rand(8, 18)):
			sequence += ch(let)

		i = inp(f"Type in this random sequence to confirm {sequence} ? ")
		if (i != sequence):
			out(f"Invalid sequence.")
			sys.exit()

		out("Wiping data.")
		for i in ["accounts", "user_data", "stats", "logged_in"]:
			s.delete_table(i)
		out("Creating template database...")
		s.add_table("accounts", {"username": s.string_unique, "password": s.string})
		s.add_table("user_data", {"username": s.string, "history": s.string, "guesses": s.integer, "word": s.string, "time": s.string, "id": s.integer})
		s.add_table("stats", {"username": s.string_unique, "right": s.integer, "wrong": s.integer})
		s.add_table("logged_in", {"username": s.string_unique})

	if args.manage_database:
		i = inp("Confirm (y/N) ? ").lower()
		i = i.replace(" ", "")
		if i == "n" or i == "":
			sys.exit()
		elif i == "y":
			os.system("cls")
		else:
			out("Invalid operation.")
			sys.exit()
		while True:
			i = input(f"{f.GREEN}database > {f.RESET}")
			if i.lower() == ".exit":
				os.system("cls")
				sys.exit()
			if ".help" in i:
				continue
			if ".system" in i:
				continue
			cmd = [f"{os.path.dirname(__file__)}\\driver\\sqlite3.exe", "data.db", "-cmd", i, ".exit"]
			run(cmd, shell=True)
			print("")

	if args.delete_game:
		if (is_logged_in(s)) == False:
			out("Not logged in.")
			sys.exit()
		user = s.get_all_data("logged_in")[0][0]
		data = s.get_data("user_data", {"username": user})
		if data == []:
			out("No games have been played.")
			sys.exit()

		formatted_data = {}
		for entry in range(len(data)):
			formatted_data[data[entry][5]] = data[entry]
		data = formatted_data

		out("Type in a game's ID to delete the game or type 'no' to exit\n")
		print(f"{f.LIGHTBLUE_EX}ENTRIES:{f.RESET}")
		for entry in range(len(data)):
			entry = entry+1
			if data[entry] == (user, "deleted", 0, "deleted", "deleted", entry):
				print(f"{f.LIGHTBLUE_EX}ID: {f.YELLOW}{entry}{f.RESET}{f.LIGHTBLUE_EX} {' ' * (8-len(str(entry)))}Status: {f.RED}GAME DELETED{f.RESET}")
			else:
				if (data[entry][2] < 7):
					print(f"{f.LIGHTBLUE_EX}ID: {f.YELLOW}{entry}{f.LIGHTBLUE_EX} {' ' * (8-len(str(entry)))}Guesses: {f.YELLOW}{data[entry][2]}{f.LIGHTBLUE_EX} {' ' * (8-len(str(data[entry][2])))}Word: {f.YELLOW}{data[entry][3]}{f.LIGHTBLUE_EX}    Time: {f.YELLOW}{data[entry][4]}{f.RESET}")
				else:
					print(f"{f.LIGHTBLUE_EX}ID: {f.YELLOW}{entry}{f.LIGHTBLUE_EX} {' ' * (8-len(str(entry)))}Guesses: {f.YELLOW}{'Wrong'}{f.LIGHTBLUE_EX} {' ' * (8-len('Wrong'))}Word: {f.YELLOW}{data[entry][3]}{f.LIGHTBLUE_EX}    Time: {f.YELLOW}{data[entry][4]}{f.RESET}")

		id = inp("ID ? ")
		if (id == "no"):
			sys.exit()
		r = re.compile("[0-9]{1,}")
		if r.match(id) == None:
			out("Invalid ID")
			sys.exit()
		if r.match(id).group() != id:
			out("Invalid ID")
			sys.exit()

		if (int(id) <= len(data)) == False or int(id) < 1:
			out("Invalid ID")
			sys.exit()

		i = inp("Confirm (y/n) ? ").lower()
		if i != "y" and i != "n":
			out("Invalid operation.")
			sys.exit()
		if i == "n":
			sys.exit()
		old_data = s.get_data("user_data", {"username": user, "id": int(id)})[0]
		s.delete_value("user_data", {"username": old_data[0], "id": int(id)})

		s.insert("user_data", {"username": user, "history": "deleted", "guesses": 0, "word": "deleted", "time": "deleted", "id": int(id)})

	if args.data:
		if args.data == "export":
			out("Gathering data...")
			shutil.copy("data.db", f"{OriginalPath}\\exported.data")
			out(f"Exported to: {OriginalPath}\\exported.data")

		if args.data == "import":
			directory = inp("Directory for exported data ? ")
			if (os.path.isfile(directory)) == False and (os.path.isfile(OriginalPath + "\\" + directory)) == False:
				out("Invalid file")
				sys.exit()
			confirm = inp("Importing will wipe your data, are you sure ? (y/n) ").lower()
			if confirm == "y":
				out("Importing...")
				shutil.copy(OriginalPath + "\\" + directory, "data.db")
			elif confirm == "n":
				out("Canceling...")
				sys.exit()
			

if __name__ == "__main__":
	main()


